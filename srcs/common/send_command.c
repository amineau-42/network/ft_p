/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   send_to_client.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 16:07:54 by amineau           #+#    #+#             */
/*   Updated: 2019/07/16 18:08:12 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

int	send_command(char* cmd, char *arg, t_env *env)
{
	int		ret;
	char 	*str;

	str = ft_straddc(cmd, ' ');
	str = ft_strcln1join(ft_strcln1join(str, arg), FTP_EOC);
	if (env->debug == true)
		ft_printf("Command : %s", str);
	if (env->ssl_activated == true)
		ret = SSL_write(env->ssl, str, ft_strlen(str));
	else
		ret = write(env->cs, str, ft_strlen(str));
	free(str);
	return (ret);
}