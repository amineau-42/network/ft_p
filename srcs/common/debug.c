/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 19:06:20 by amineau           #+#    #+#             */
/*   Updated: 2019/07/16 23:32:13 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

void	debug(t_env* env)
{
	char 	buff[BUFF_SIZE];
	char	*str;
	int		r;
	int		ret;

	ft_putendl("DEBUG: open");
	while((r = read(STDIN_FILENO, buff, BUFF_SIZE)))
	{
		buff[r - 1] = '\0';
		if (!ft_strcasecmp("end", buff)){
			ft_putendl("DEBUG: exit");
			break;
		}
		str = ft_strjoin(buff, FTP_EOC);
		if (env->ssl_activated == true)
			ret = SSL_write(env->ssl, str, ft_strlen(str));
		else
			ret = write(env->cs, str, ft_strlen(str));
		free(str);
	}
}

void	print_env(t_env* env)
{
	ft_printf("env :\n\twdir : %s\n\tssl_activated : %d\n\tdebug : %d\n",
				env->wdir, env->ssl_activated, env->debug);
}