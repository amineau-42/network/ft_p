/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_generic.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 19:06:20 by amineau           #+#    #+#             */
/*   Updated: 2019/07/17 22:56:05 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

void	put_req_arg(char *cmd)
{
	printf("Argument is required for this command [%s]\n", cmd);
}

void	put_no_req_arg(char *cmd)
{
	printf("This command [%s] is used without arguments\n", cmd);
}

char	*logout(t_client_verbs *cv, int sock)
{
	(void)sock;
	if (cv->cv_arg)
	{
		put_no_req_arg(cv->cv_verb);
		return (NULL);
	}
	return (ft_strdup(LOGOUT));
}

char	*help(t_client_verbs *cv, int sock)
{
	(void)cv;
	(void)sock;
	size_t	i;

	i = 0;
	ft_printf("Available commands");
	while(g_user_cmd_str[i])
		ft_printf("%s\n", g_user_cmd_str[i++]);
	return (NULL);
}

char	*print_workdir(t_client_verbs *cv, int sock)
{
	(void)sock;
	if (cv->cv_arg)
	{
		put_no_req_arg(cv->cv_verb);
		return (NULL);
	}
	return (ft_strdup(PRINT_WORKDIR));
}