/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer_parser.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 19:06:20 by amineau           #+#    #+#             */
/*   Updated: 2019/07/17 23:00:31 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

// TODO : Merge with ftp_lexer (server.c)
int	user_lexer(const char *buff, t_client_verbs* cv)
{
	char	**split;
	int		code_command;

	split = ft_strsplit(buff, ' ');
	if (!split[0])
		return (-1);
	else if ((code_command = ft_arraystr(g_user_cmd_str, split[0])) == -1)
	{
		printf("Unkwown command : [%s]\nType help for more information\n", buff);
		return (-1);
	}
	cv->cv_verb = split[0];
	cv->cv_arg = split[1];
	cv->cv_code = code_command;
	return (0);
}

// TODO : Merge with ftp_parser (server.c)
char*	user_parser(t_client_verbs* cv, int sock)
{
	t_client_action	command[] = {
		list, change_workdir, get_file, put_file, print_workdir, logout, help
	};

	return (command[cv->cv_code](cv, sock));
}