/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 19:06:20 by amineau           #+#    #+#             */
/*   Updated: 2019/07/18 01:14:03 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

void	usage(char *str)
{
	printf("Usage: %s <host> <port> [-u <user name>] [-d <debug>] -\n", str);
	exit(-1);
}

struct in_addr	htoaddr(char *name)
{
	struct hostent	*host;
	struct in_addr	addr;

	host = gethostbyname(name);
	if (!host)
	{
		if (h_errno == HOST_NOT_FOUND)
			printf("%s is unknown\n", name);
		else if (h_errno == NO_DATA)
			printf("%s does not have an IP address\n", name);
		else if (h_errno == NO_RECOVERY)
			printf("Server error\n");
		else if (h_errno == TRY_AGAIN)
			printf("A temporary error occurred on an authoritative name server.  Try again later.\n");
		exit(-1);
	}
	ft_memcpy(&addr.s_addr, host->h_addr, host->h_length);
	return(addr);
}

int		create_client(struct in_addr host, int port)
{
	int					sock;
	struct protoent		*proto;
	struct sockaddr_in	sin;

	proto = getprotobyname("tcp");
	if (!proto)
		exit(-1);
	sock = socket(AF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr = host;
	if (connect(sock, (const struct sockaddr *)&sin, sizeof(sin)) == -1)
	{
		if (errno == EADDRINUSE)
			printf("Local address is already in use\n");
		else if (errno == ECONNREFUSED)
			printf("Remote address not listening\n");
		else
			printf("Connect failed\n");
		exit(-1);
	}
	printf("Client connected\n");
	return(sock);
}

void	getargs(int ac, char** av, t_client_args *ca)
{
	char opt;

	if (ac < 3)
		usage(av[0]);
	ca->ca_host = htoaddr(av[1]);
	ca->ca_port = ft_atoi(av[2]);
	ca->ca_user = NULL;
	ca->debug = false;
	while ((opt = (char)getopt(ac, av, "ud")) != -1)
	{
		if (opt == 'u')
			ca->ca_user = av[optind];
		else if (opt == 'd')
			ca->debug = true;
		else
			usage(av[0]);
	}
}

size_t	listen_server(char	*buff, t_env *env)
{
	size_t	r;

	while ((r = received(env, buff)) <= 0)
		;
	return (r);
}

void	disconnect(t_env * env)
{
	if (env->ssl_activated)
	{
		SSL_free(env->ssl);
		SSL_CTX_free(env->ctx);
		cleanup_openssl();
	}
	close(env->cs);
	printf("Client disconnected\n");
}

int		main(int ac, char **av)
{
	char	*buff;
	int		gnllen;
	t_client_args	ca;
	t_client_verbs	cv;
	char*	cmd;
	t_env	env;

	getargs(ac, av, &ca);
	
	env.cs = create_client(ca.ca_host , ca.ca_port);
	env.debug = ca.debug;
	env.ssl = NULL;
	env.ctx = NULL;
	env.ssl_activated = false;
	env.wdir = NULL;

	connection_protocol(&env);
	connection_user(&ca, &env);

	while((gnllen = get_next_line(STDIN_FILENO, &buff)) > 0)
	{
		if (user_lexer(buff, &cv) != -1
			&& (cmd = user_parser(&cv, env.cs)))
		{
			send_to_server(cmd, "", &env); // TODO: change return of user_parser to struc with callbacks
			free(cmd);
		}
	}
	disconnect(&env);
	// TODO : Split .h to remove these lines
	(void)g_user_cmd_str;
	(void)g_ftp_cmd_str;
	(void)g_ftp_code_str;
	return (0);
}
