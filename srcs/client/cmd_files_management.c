/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_files_management.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 19:06:20 by amineau           #+#    #+#             */
/*   Updated: 2019/07/17 22:58:49 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

char	*list(t_client_verbs *cv, int sock)
{
	(void)sock;
	return (ft_strcjoin(LIST, cv->cv_arg, ' '));
}

char	*change_workdir(t_client_verbs *cv, int sock)
{
	(void)sock;
	char	*cmd;

	if (!cv->cv_arg)
		cmd = ft_strdup(CHANGE_TO_PARENT_DIR);
	else
		cmd = ft_strcjoin(CHANGE_WORKDIR, cv->cv_arg, ' ');
	return (cmd);
}

char	*get_file(t_client_verbs *cv, int sock)
{
	(void)sock;
	if (!cv->cv_arg)
	{
		put_req_arg(cv->cv_verb);
		return (NULL);
	}
	return (ft_strcjoin(RETRIEVE, cv->cv_arg, ' '));
}

char	*put_file(t_client_verbs *cv, int sock)
{
	(void)sock;
	if (!cv->cv_arg)
	{
		put_req_arg(cv->cv_verb);
		return (NULL);
	}
	return (ft_strcjoin(STORE, cv->cv_arg, ' '));
}