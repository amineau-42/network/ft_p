/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   protocole_connection.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amineau <amineau@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 19:06:20 by amineau           #+#    #+#             */
/*   Updated: 2019/07/18 01:32:02 by amineau          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_p.h"

t_bool		SSL_connection(t_env *env)
{
	SSL_CTX *ctx;
	SSL		*ssl;

	init_openssl();
	ctx = create_client_context();
	ssl = SSL_new(ctx);
	SSL_set_fd(ssl, env->cs);

	if (SSL_connect(ssl) == -1)
	{
		ft_putendl("Caution ! Server doesn't accept TLS connection");
		return (false);
	}
	env->ctx = ctx;
	env->ssl = ssl;
	env->ssl_activated = true;
	return (true);
}			

typedef struct	s_instruction
{
	char	*verb;
	char	*arg;
	void	(*success)(t_env * env);
	void	(*error)(t_env * env);
	void	(*failure)(t_env * env);
}				t_instruction;

t_instruction	*init_instruction(char* verb, char *arg)
{
	t_instruction	*ins;

	if (!(ins = (t_instruction*)malloc(sizeof(t_instruction))))
		exit(EXIT_FAILURE); // TODO: exit 
	ins->verb = verb;
	ins->arg = arg;
	return (ins);
}

t_instruction	*auth_success(t_env *env)
{
	SSL_CTX *ctx;
	SSL		*ssl;

	init_openssl();
	ctx = create_client_context();
	ssl = SSL_new(ctx);
	SSL_set_fd(ssl, env->cs);

	if (SSL_connect(ssl) == -1)
	{
		ft_putendl("Caution ! Server doesn't accept TLS connection");
		exit(EXIT_FAILURE); // TODO: exit 
	}
	env->ctx = ctx;
	env->ssl = ssl;
	env->ssl_activated = true;
	return (NULL);
}

t_instruction	*auth_error(t_env *env)
{
	t_instruction	*ins;

	ins = init_instruction(AUTH_METHOD);
	return ins;
}

void		connection_protocol(t_env *env)
{
	t_bool	success;
	char	buff[BUFF_SIZE];
	const char	*method[] = {"TLS", "SSL", NULL};
	size_t		i;
	t_instruction	*ins;

	success = false;

	ins = init_instruction(AUTH_METHOD);

	// build AUTH TLS
	// prepare callbacks (success, fail and error)
	// return build
	i = 0;
	while (ins && method[i])
	{
		send_to_server(ins.verb, ins.arg, env);
		listen_server(buff, env);
		if (buff[0] == '2')
			ins.success(env);
		else if (buff[0] == '1')
			ins.error(env);
		else
			ins.failure(env);

	}

	if (SSL_connection(env))
	{
		while(success == false)
		{
			listen_server(buff, env);
			if (buff[0] == '2')
				success = true;
			if (env->debug == true)
				ft_printf("Receiced from server : %s\n", buff);
		}
	}
}

size_t	get_password(char *password)
{
    static struct termios old_terminal;
    static struct termios new_terminal;
	size_t	r;

    tcgetattr(STDIN_FILENO, &old_terminal);

    new_terminal = old_terminal;
    new_terminal.c_lflag &= ~(ECHO);

    tcsetattr(STDIN_FILENO, TCSANOW, &new_terminal);

    r = read(STDIN_FILENO, password, BUFF_SIZE - 1);
    tcsetattr(STDIN_FILENO, TCSANOW, &old_terminal);
	return (r);
}

void	connection_user(t_client_args *ca, t_env *env)
{
	size_t	r;
	char user[BUFF_SIZE];
	t_bool	success;
	char	response[BUFF_SIZE];

	
	success = false;
	while (success == false){
		
		if (ca->ca_user == NULL)
		{
			ft_printf("user : ");
			r = read(STDIN_FILENO, user, BUFF_SIZE - 1);
			user[r - 1] = '\0';
			send_to_server(USERNAME, user, env);
		}
		else
		{
			send_to_server(USERNAME, ca->ca_user, env);
		}
		listen_server(response, env);
		if (response[0] == '3')
		{
			char password[BUFF_SIZE];
			ft_printf("pass : ");
			r = get_password(password);
			password[r - 1] = '\0';
			send_to_server(PASSWORD, password, env);
			
			listen_server(response, env);
			if (response[0] == '2')
				success = true;
			ft_printf("%s\n", response);
		}
	}
}
