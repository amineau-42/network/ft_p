//SSL-Client.c
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>
#include <resolv.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

int create_socket(int port)
{
    int s;
    struct sockaddr_in addr;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
	perror("Unable to create socket");
	exit(EXIT_FAILURE);
    }

    if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
	perror("Unable to bind");
	exit(EXIT_FAILURE);
    }

    if (listen(s, 1) < 0) {
	perror("Unable to listen");
	exit(EXIT_FAILURE);
    }

    return s;
}

void init_openssl()
{ 
    SSL_load_error_strings();	
    OpenSSL_add_ssl_algorithms();
}

void cleanup_openssl()
{
    EVP_cleanup();
}

SSL_CTX *create_context()
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    method = SSLv23_server_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
	perror("Unable to create SSL context");
	ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    return ctx;
}

void configure_context(SSL_CTX *ctx)
{
    SSL_CTX_set_ecdh_auto(ctx, 1);

    /* Set the key and cert */
    if (SSL_CTX_use_certificate_file(ctx, "server.crt", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "server.key", SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv)
{
    int sock;
    SSL_CTX *ctx;

    init_openssl();
    ctx = create_context();

    configure_context(ctx);

    sock = create_socket(4433);

    /* Handle connections */
    while(1) {
        struct sockaddr_in addr;
        size_t len = sizeof(addr);
        SSL *ssl;
        const char reply[] = "220\r\n";

        int client = accept(sock, (struct sockaddr*)&addr, &len);
        if (client < 0) {
            perror("Unable to accept");
            exit(EXIT_FAILURE);
        }

        ssl = SSL_new(ctx);
        SSL_set_fd(ssl, client);

        printf("SSL_accept : %d\n", SSL_accept(ssl));
        if (SSL_accept(ssl) <= 0) {
            ERR_print_errors_fp(stderr);
        }
        else {
            SSL_write(ssl, reply, strlen(reply));
        }

        SSL_free(ssl);
        close(client);
    }

    close(sock);
    SSL_CTX_free(ctx);
    cleanup_openssl();
}


///////////////////////////////////////////
///////////////////////////////////////////

// //SSL-Server.c
// #include <errno.h>
// #include <unistd.h>
// #include <malloc.h>
// #include <string.h>
// #include <arpa/inet.h>
// #include <sys/socket.h>
// #include <sys/types.h>
// #include <netinet/in.h>
// #include <resolv.h>
// #include "openssl/ssl.h"
// #include "openssl/err.h"

// #define FAIL    -1

// int OpenListener(int port)
// {   int sd;
//     struct sockaddr_in addr;

//     sd = socket(PF_INET, SOCK_STREAM, 0);
//     bzero(&addr, sizeof(addr));
//     addr.sin_family = AF_INET;
//     addr.sin_port = htons(port);
//     addr.sin_addr.s_addr = INADDR_ANY;
//     if ( bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
//     {
//         perror("can't bind port");
//         abort();
//     }
//     if ( listen(sd, 10) != 0 )
//     {
//         perror("Can't configure listening port");
//         abort();
//     }
//     return sd;
// }

// int isRoot()
// {
//     if (getuid() != 0)
//     {
//         return 0;
//     }
//     else
//     {
//         return 1;
//     }

// }
// SSL_CTX* InitServerCTX(void)
// {   SSL_METHOD *method;
//     SSL_CTX *ctx;

//     OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
//     SSL_load_error_strings();   /* load all error messages */
//     method = TLSv1_2_server_method();  /* create new server-method instance */
//     ctx = SSL_CTX_new(method);   /* create new context from method */
//     if ( ctx == NULL )
//     {
//         ERR_print_errors_fp(stderr);
//         abort();
//     }
//     return ctx;
// }

// void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile)
// {
//     /* set the local certificate from CertFile */
//     if ( SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0 )
//     {
//         ERR_print_errors_fp(stderr);
//         abort();
//     }
//     /* set the private key from KeyFile (may be the same as CertFile) */
//     if ( SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0 )
//     {
//         ERR_print_errors_fp(stderr);
//         abort();
//     }
//     /* verify private key */
//     if ( !SSL_CTX_check_private_key(ctx) )
//     {
//         fprintf(stderr, "Private key does not match the public certificate\n");
//         abort();
//     }
// }

// void ShowCerts(SSL* ssl)
// {   X509 *cert;
//     char *line;

//     cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */
//     if ( cert != NULL )
//     {
//         printf("Server certificates:\n");
//         line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
//         printf("Subject: %s\n", line);
//         free(line);
//         line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
//         printf("Issuer: %s\n", line);
//         free(line);
//         X509_free(cert);
//     }
//     else
//         printf("No certificates.\n");
// }

// void Servlet(SSL* ssl) /* Serve the connection -- threadable */
// {   char buf[1024];
//     char reply[1024];
//     int sd, bytes;
//     const char* HTMLecho="<html><body><pre>%s</pre></body></html>\n\n";

//     if ( SSL_accept(ssl) == FAIL )     /* do SSL-protocol accept */
//         ERR_print_errors_fp(stderr);
//     else
//     {
//         ShowCerts(ssl);        /* get any certificates */
//         bytes = SSL_read(ssl, buf, sizeof(buf)); /* get request */
//         if ( bytes > 0 )
//         {
//             buf[bytes] = 0;
//             printf("Client msg: \"%s\"\n", buf);
//             sprintf(reply, HTMLecho, buf);   /* construct reply */
//             SSL_write(ssl, reply, strlen(reply)); /* send reply */
//         }
//         else
//             ERR_print_errors_fp(stderr);
//     }
//     sd = SSL_get_fd(ssl);       /* get socket connection */
//     SSL_free(ssl);         /* release SSL state */
//     close(sd);          /* close connection */
// }

// int main(int count, char *strings[])
// {   SSL_CTX *ctx;
//     int server;
//     char *portnum;

//     if(!isRoot())
//     {
//         printf("This program must be run as root/sudo user!!");
//         exit(0);
//     }
//     if ( count != 2 )
//     {
//         printf("Usage: %s <portnum>\n", strings[0]);
//         exit(0);
//     }
//     SSL_library_init();

//     portnum = strings[1];
//     ctx = InitServerCTX();        /* initialize SSL */
//     LoadCertificates(ctx, "mycert.pem", "mycert.pem"); /* load certs */
//     server = OpenListener(atoi(portnum));    /* create server socket */
//     while (1)
//     {   struct sockaddr_in addr;
//         socklen_t len = sizeof(addr);
//         SSL *ssl;

//         int client = accept(server, (struct sockaddr*)&addr, &len);  /* accept connection as usual */
//         printf("Connection: %s:%d\n",inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
//         ssl = SSL_new(ctx);              /* get new SSL state with context */
//         SSL_set_fd(ssl, client);      /* set connection socket to SSL state */
//         Servlet(ssl);         /* service connection */
//     }
//     close(server);          /* close server socket */
//     SSL_CTX_free(ctx);         /* release context */
// }